package com.mei;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AopurlApplication {
    public static void main(String[] args) {
        SpringApplication.run(AopurlApplication.class,args);
    }
}
